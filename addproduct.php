<?php
ini_set('display_errors', 1); ini_set('log_errors',1); error_reporting(E_ALL); mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

    include 'dba.inc.php';
    include 'product.inc.php';
    
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Product Add</title>
        <link href="style.css" rel="stylesheet"></link>
    </head>

    <body>

      <header>
       <p> Product add</p>
        <a href="productlist.php">
          <button>Cancel</button>
        </a>
      </header>
    
        <?php
        echo "<form method ='POST' action='".setProducts($conn)."' id ='product_form'>
        <label for='sku'>SKU</label>
        <input type='text' name='sku' id='sku' required><br>
  
        <label for='name'>Name</label>
        <input type='text' name='name' id='name' required><br>
  
        <label for='price'>Price ($)</label >
        <input type='text' name='price' id='price' required><br>
        <select id='productType' onchange='typeChange()'>
          <option value='Select'>Select</option>
          <option value='DVD'>DVD</option>
          <option value='Furniture'>Furniture</option>
          <option value='Book' id='Book'>Book</option>
        </select><br>
  
        <div class='hidden' id='dvd'>
          <label for='size'>Size (MB)</label><br>
          <input type='text' name='size' id='size'><br>
          <p>Please provide size in MB</p>
        </div>
  
        <div class='hidden' id='book'>
          <label for='weight'>Weight (KG)</label>
          <input type='text' name='weight' id='weight'><br>
          <p>Please provide weight in KG</p>
        </div>
  
        <div class='hidden' id='furniture'>
          <label for='height'>Height (CM)</label>
          <input type='text' name='height' id='height'><br>
  
          <label for='width'>Width (CM)</label>
          <input type='text' name='width' id='width'><br>
  
          <label for='length'>Length (CM)</label>
          <input type='text' name='length' id='length'><br>
          <p>Please provide dimensions in HxWxL</p>
        </div>
        
        
        <button type='submit' name='productSave'>Save</button>
       
   
        </form>";

        ?>

       
    </body>
    <script src="script.js"></script>
</html>