function typeChange() {
    // add back all class 'hidden' for all elements found by their IDs
    document.getElementById('dvd').classList = ['hidden'];
    document.getElementById('book').classList = ['hidden'];
    document.getElementById('furniture').classList = ['hidden'];    
 
    let productType = document.getElementById("productType").value;
 
    // remove 'hidden' only for the one
     if (productType === "DVD") {
        document.getElementById('dvd').classList.remove('hidden');
     } else if (productType === "Furniture") {
       document.getElementById('furniture').classList.remove('hidden');
     } else if (productType === "Book") {
         document.getElementById('book').classList.remove('hidden');
     }
 }
